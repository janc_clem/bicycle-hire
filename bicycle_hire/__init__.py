import os

current_dir = os.path.abspath(os.path.dirname(__file__))
db_path = os.path.join(current_dir, 'db.sqlite')
