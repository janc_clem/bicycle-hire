import pytest

from bicycle_hire.app import create_app

# Executed once before all tests
@pytest.yield_fixture(scope='session')
def app():
    parameters = {
        'DEBUG': False,
        'TESTING': True,
    }

    _app = create_app(settings_override=parameters)

    ctx = _app.app_context()
    ctx.push()

    yield _app

    ctx.pop()

# Executed for each test (sets up an isolated client)
@pytest.yield_fixture(scope='function')
def client(app):

    yield app.test_client()