from flask import url_for

# Tests whether the pages can be accessed
class TestBicycleViews(object):
    def test_index_page(self, client):
        response = client.get(url_for('bicycles.index'))
        assert response.status_code == 200

    def test_booking_page(self, client):
        response = client.get(url_for('bicycles.booking'))
        assert response.status_code == 200