from flask import (Blueprint,
                   render_template,
                   request,
                   flash,
                   redirect,
                   url_for,
                   session
                   )
from flask_login import (current_user,
                         login_required
                        )
from bicycle_hire.blueprints.user.helpers.form_validation import flash_errors
from bicycle_hire.blueprints.bicycle.forms import FilterForm, BookingFilterForm, AuthForm
from bicycle_hire.blueprints.bicycle.models import Bicycle, Location, Booking, Form
from bicycle_hire.blueprints.payment.models import Payment
from bicycle_hire.blueprints.bicycle.map import make_map
from bicycle_hire.populate_database import populate
from bicycle_hire.blueprints.bicycle.helpers.form_prefill import prefill_booking_form
import datetime

bicycles = Blueprint('bicycles', __name__, template_folder='templates')


@bicycles.route('/', methods = ['GET', 'POST'])
def index():
    # populate()

    # Remove timed-out bicycle reservations
    # Error handling for the case when db tables are not set-up
    try:
        Payment.payment_timeout()
    except:
        pass

    # Render the Google Map with location markers for the bicycle stores
    try:
        bike_map = make_map()
    except:
        bike_map = None

    # The bicycle search form object
    form = FilterForm()

    if form.validate_on_submit():
        location = form.location.data
        start_time = form.start_time.data
        end_time = form.end_time.data
        start_date = form.start_date.data
        end_date = form.end_date.data

        # Converts both the times and dates into dateTime format
        start = datetime.datetime.combine(start_date, datetime.time(int(start_time)))
        end = datetime.datetime.combine(end_date, datetime.time(int(end_time)))

        # Get the number of available bicycles
        query = Booking.available_bikes(location, start, end)
        available_bikes = len(query)
        chosen_location = str(form.location.data).capitalize()

        # Display the search results as a flash message
        flash('The number of bikes available in ' + chosen_location +
                ' at time ' + str(start) + ' to ' + str(end) +
                ' is: ' + str(available_bikes) + ' bikes',
                category="info")

        # Create a new map to reflect the available bikes in the given timeframe
        bike_map = make_map(start, end)

        # Save the form data for prefill and redirect to booking
        if form.submit.data:
            form_model = Form(location, start, end)
            form_model.commit_form()
            form_id = form_model.id
            return redirect(url_for('bicycles.booking', form_id=form_id))
    else:
        flash_errors(form, 'danger')

    return render_template('bicycle/index.html',
                            bicycles=bicycles,
                            bike_map=bike_map,
                            form=form
                           )


@bicycles.route('/booking', methods = ['GET', 'POST'])
def booking():
    form = BookingFilterForm()
    payment_id = request.args.get('payment_id')
    form_id = request.args.get('form_id')

    # Prefill the form with current booking's data
    if form_id and request.method == 'GET':
        form_model = Form.get_by_id(form_id)
        prefill_booking_form(form, form_model, True, True)

    if form.validate_on_submit():
        location = form.location.data
        # Converts both the times and dates into dateTime format
        start = datetime.datetime.combine(form.start_date.data,
                        datetime.time(int(form.start_time.data)))
        end = datetime.datetime.combine(form.end_date.data,
                        datetime.time(int(form.end_time.data)))

        # Retrieves the number of requested bikes from form as well as
        # the set of available bikes at the location given in the form
        available_bikes = Booking.available_bikes(form.location.data,
                                                 start,end)
        number_of_bikes = form.number_of_bikes.data

        # Update the form model if possible, otherwise create a new one
        if form_id:
            form_model = Form.get_by_id(form_id)
            if current_user.is_authenticated:
                form_model.update_form(form, number_of_bikes, current_user.email)
            else:
                form_model.update_form(form, number_of_bikes)
        else:
            if current_user.is_authenticated:
                form_model = Form(location, start, end, number_of_bikes, current_user.email)
            else:
                form_model = Form(location, start, end, number_of_bikes)
            form_model.commit_form()
            form_id = form_model.id

        # Update an existing booking if possible, create a new one otherwise
        if payment_id:
            # Get the payment instance
            payment_obj = Payment.retrieve_payment_record(payment_id)
            # Get the corresponding booking instance
            booking = Booking.get_booking_record(payment_obj.get_booking_id())
            # Update the booking record with the new form data
            booking.update_from_form(form)
            # Update the email with the current user's
            if current_user.is_authenticated:
                booking.update_email(current_user.email)

            return redirect(url_for('payments.payIndex',
                                    payment_id=payment_id,
                                    form_id=form_id))
        else:
            email = 'temp@temp.com'
            # Gets current user's email
            if current_user.is_authenticated:
                email = current_user.email

            # Proceeds to create a booking object
            new_booking = Booking(start,end,email)

            # Adds the reserved bikes to the booking record
            for i in range(number_of_bikes):
                new_booking.bicycles.append(available_bikes.pop())

            # Booking record added to database
            new_booking.make_booking()
            cost = new_booking.calc_cost()

            # Payment record with null pay value added to database
            new_payment = Payment(new_booking.id,cost,False)
            new_payment.make_payment()

            payment_id = new_payment.id

            if current_user.is_authenticated:
                return redirect(url_for('payments.payIndex',
                                        payment_id=payment_id,
                                        form_id=form_id))

            return redirect(url_for('bicycles.auth',
                                payment_id=payment_id,
                                form_id=form_id))
    else:
        flash_errors(form, 'danger')

    return render_template('bicycle/filter.html',
                            booking_form=form,
                            form_id=form_id,
                            payment_id=payment_id
                           )


@bicycles.route('/auth', methods = ['GET', 'POST'])
def auth():
    # Url params
    payment_id = request.args.get('payment_id')
    form_id = request.args.get('form_id')

    # If the user is already signed in
    if current_user.is_authenticated:
        flash("You are already signed in!", "info")
        return redirect(url_for('bicycles.booking',
                                payment_id=payment_id,
                                form_id=form_id))

    form = AuthForm()

    # Payment Information
    payment_obj = Payment.retrieve_payment_record(payment_id)

    # Booking Information
    booking = Booking.get_booking_record(payment_obj.get_booking_id())

    # Prefill the form with current email data
    if form_id and request.method == 'GET':
        form_model = Form.get_by_id(form_id)
        prefill_booking_form(form, form_model, False, False, True)

    if form.validate_on_submit():

        # Updates booking record with given email
        email = form.email.data
        booking.update_email(email)

        # Update form model with the new email
        if form_id:
            form_model = Form.get_by_id(form_id)
            form_model.update_form(None, None, form.email.data)

        return redirect(url_for('payments.payIndex',
                                payment_id = payment_id,
                                form_id = form_id))
    else:
        flash_errors(form)

    return render_template('bicycle/auth.html',
                           form=form,
                           payment_id=payment_id,
                           form_id=form_id)
