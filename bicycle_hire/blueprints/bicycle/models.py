from bicycle_hire.extensions import db
from sqlalchemy.ext.declarative import declarative_base
import datetime

HOURLY_RATE = 5
DISCOUNT = 0.2


# Many to Many table between Bicycle and Booking
association_table = db.Table('association', db.metadata,
    db.Column('booking_id', db.Integer, db.ForeignKey('bookings.id', ondelete='cascade')),
    db.Column('bicycle_id', db.Integer, db.ForeignKey('bicycles.id', ondelete='cascade'))
)

class Bicycle(db.Model):
    __tablename__ = 'bicycles'


    # Table attributes
    id = db.Column(db.Integer, primary_key=True)
    location_id = db.Column(db.Integer,
                            db.ForeignKey('locations.id'),
                            nullable=False)

    bookings = db.relationship('Booking',
                                secondary = 'association',
                                back_populates = 'bicycles')

    # Initialises an instance of Bicycle
    def __init__(self, location_id):
        self.location_id = location_id

    # Displayed when printing the object
    def __repr__(self):
        return "Bicycle_id:{0}".format(self.id)

    def get_loc_id(self):
        return self.location_id

    @staticmethod
    def bicycles_by_location_count():
        filtered = {}

        # Uses the location name as a key, the value
        # is a corresponding number of bicycles
        locations = Location.query.all()
        for location in locations:
            filtered[location.name] = len(location.bicycles)
        return filtered

    @staticmethod
    def bicycles_by_location():
        filtered = {}

        # Uses the location name as a key, the value
        # is a corresponding number of bicycles
        locations = Location.query.all()
        for location in locations:
            filtered[location.name] = location.bicycles
        return filtered


class Location(db.Model):
    __tablename__ = 'locations'

    # Table Relationships
    bicycles = db.relationship('Bicycle', backref='location')

    # Table Attributes
    id = db.Column(db.Integer, primary_key=True)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    name = db.Column(db.String(100), unique=True, index=True)

    # Initialises an instance of Location
    def __init__(self, latitude, longitude, name):
        self.latitude = latitude
        self.longitude = longitude
        self.name = name

    # Displayed when printing the object
    def __repr__(self):
        return "Location: {0}.\nLatitude: {1:.2f}, Longitude: {2:.2f}".format(
                                     self.name, self.latitude, self.longitude)

    def get_loc_name(self):
        return self.name

    @staticmethod
    def get_location_record(id):
        return Location.query.filter(Location.id == id).first()

    # Returns a dictionary of using location.name as the key
    # and a tuple of (latitude, longitude) as the value
    @staticmethod
    def loc_coords():
        coords = {}

        # Using the location name as a key, the value
        # is the latitude and longitude
        locations = Location.query.all()
        for location in locations:
            coords[location.name] = (location.latitude, location.longitude)

        return coords

    @staticmethod
    def list_locations():
        # this module returns the names of the locations in a list
        loc_names = []
        locations = Location.query.all()

        for location in locations:
            loc_names.append((location.name.lower(), location.name))
        return loc_names


class Booking(db.Model):
    __tablename__ = 'bookings'

    # Table Relationships
    bicycles = db.relationship('Bicycle',
                               secondary = 'association',
                               back_populates = 'bookings')
    payment = db.relationship('Payment', backref = 'bookings', uselist=False)

    #Table Attributes
    id = db.Column(db.Integer, primary_key = True)
    start_date_time = db.Column(db.DateTime, nullable = False, index = True)
    end_date_time = db.Column(db.DateTime, nullable = False, index = True)
    email = db.Column(db.String, nullable = False)

    # Function that checks if booking is eligible for discount
    def is_eligible(self):

        time_diff = self.time_in_seconds()

        # If renting bike(s) for over 8 hours (1 working day)
        # eligible for discount
        if time_diff > 28800 or time_diff == 28800:
            return True
        return False

    # Calculates the cost of the booking
    def calc_cost(self):

        num_of_bikes = len(self.bicycles)
        booking_length = self.time_in_seconds()
        booking_hours = round(booking_length / 3600)

        # Cost of booking without discount
        cost = booking_hours * num_of_bikes * HOURLY_RATE

        # Discounts total cost by the DISCOUNT constant's amount, if eligible
        if self.is_eligible():
            cost = cost * (1 - DISCOUNT)
        
        return cost

    # Function to convert the booking duration into seconds
    def time_in_seconds(self):
        #Retrieves booking start and end time
        start = self.start_date_time
        end = self.end_date_time

        # Converts the time difference into seconds
        time_diff = start - end
        time_diff = abs(time_diff)
        time_diff = time_diff.total_seconds()

        return time_diff

    # Function that will add a record to the booking table
    # and update the bike table with booking id (automatically?)
    def make_booking(self):

        # Updates the database with this record
        db.session.add(self)
        db.session.commit()

    @staticmethod
    def get_bike_stats(start,end):
        bike_dict = {}
        locations = Location.query.all()

        # Initialises the dictionary with numbers of zero initially
        for location in locations:
            bike_dict[location.name] = 0

        # Iterates through each booking and adds the number of booked bikes
        # to the sum of bikes booked at that location
        bookings = Booking.query.all()
        for booking in bookings:

            if within_date(start,end,booking.start_date_time.date(),booking.end_date_time.date()) == True:

                # Booking location details
                booking_loc_id = (booking.bicycles[0]).location_id
                booking_loc = Location.query.filter(Location.id == booking_loc_id).first()

                # Updates the number of bikes booked at the location
                current_sum = bike_dict[booking_loc.name]
                current_sum += len(booking.bicycles)
                bike_dict[booking_loc.name] = current_sum

        return bike_dict



    # Initialises an instance of Bicycle
    def __init__(self,start_date_time,end_date_time,email):
        self.start_date_time = start_date_time
        self.end_date_time = end_date_time
        self.email = email

    # Displayed when printing the object
    def __repr__(self):
        return """Booking Information\nEmail: {0}\nBegin: {1}\nEnd:
                {2}\n Bicycle(s): {3}""".format(self.email, self.start_date_time,
                                            self.end_date_time, self.bicycles)

    def get_email(self):
        return self.email

    def get_start(self):
        return self.start_date_time

    def get_end(self):
        return self.end_date_time

    def get_bikes(self):
        return self.bicycles

    # Updates the booking record with the given email
    def update_email(self,email):
        self.email = email
        db.session.commit()

    # Updates the booking instance given the filter form
    def update_from_form(self, form):
        start = datetime.datetime.combine(form.start_date.data,
                        datetime.time(int(form.start_time.data)))
        end = datetime.datetime.combine(form.end_date.data,
                        datetime.time(int(form.end_time.data)))
        available_bikes = Booking.available_bikes(form.location.data,
                                                 start,end)
        number_of_bikes = form.number_of_bikes.data

        # Update the start and end date times
        self.start_date_time = start
        self.end_date_time = end

        # Clear all the booked bicycles
        self.bicycles.clear()

        # Add the reserved bikes to the booking record
        for i in range(number_of_bikes):
            self.bicycles.append(available_bikes.pop())

        # Recalculate the cost of this booking, update the payment instance
        self.payment.update_cost(self.calc_cost())

        db.session.commit()

    # Returns a booking object based on a given id
    @staticmethod
    def get_booking_record(id):
        return Booking.query.filter(Booking.id == id).first()

    # Returns the number of available bikes on a given date and location
    @staticmethod
    def available_bikes(selected_loc,start_date, end_date):
        unavailable_bikes = set()
        booking_ids = []

        #Retrieves all bikes at all all locaitons
        bike_list = Bicycle.bicycles_by_location()

        # Creates a list of bikes at the input location
        total_bikes = bike_list[selected_loc]
        total_bikes_set = set(total_bikes)

        # Location object of requested location
        loc = Location.query.filter(Location.name == selected_loc).first()

        # Retrive all booking records
        all_bookings = Booking.query.all()

        # Retrives the start and end time for each booking and tests them
        for booking_record in all_bookings:
            booked_start = booking_record.start_date_time
            booked_end = booking_record.end_date_time

            #If the requested start date falls between another booking time
            # or if the requested end date falls between another booking time
            if(start_date > booked_start and start_date < booked_end):
                for bike in booking_record.bicycles:
                    unavailable_bikes.add(bike)
            elif(end_date > booked_start and end_date < booked_end):
                for bike in booking_record.bicycles:
                    unavailable_bikes.add(bike)
            elif(start_date == booked_start or start_date == booked_end):
                for bike in booking_record.bicycles:
                    unavailable_bikes.add(bike)
            elif(end_date == booked_start or end_date == booked_end):
                for bike in booking_record.bicycles:
                    unavailable_bikes.add(bike)

        # Removes the relevant bikes from the total bikes
        # available at our location
        for bike in unavailable_bikes:
            total_bikes_set.discard(bike)

        return total_bikes_set


class Form(db.Model):
    __tablename__ = 'forms'

    # Table attributes
    id = db.Column(db.Integer, primary_key = True)
    location_name = db.Column(db.String(100))
    start_date_time = db.Column(db.DateTime)
    end_date_time = db.Column(db.DateTime)
    number_of_bikes = db.Column(db.Integer)
    email = db.Column(db.String)

    def __init__(self, loc, start, end, no_bikes=None, email=None):
        self.location_name = loc
        self.start_date_time = start
        self.end_date_time = end
        self.number_of_bikes = no_bikes
        self.email = email

    # Updates all the fields corresponding to the provided arguments
    def update_form(self, form=None, no_bikes=None, email=None):
        if form:
            start_time = form.start_time.data
            end_time = form.end_time.data
            start_date = form.start_date.data
            end_date = form.end_date.data

            self.location_name = form.location.data
            self.start_date_time = datetime.datetime.combine(start_date, datetime.time(int(start_time)))
            self.end_date_time = datetime.datetime.combine(end_date, datetime.time(int(end_time)))
        if no_bikes:
            self.number_of_bikes = int(no_bikes)
        if email:
            self.email = email

        db.session.commit()

    # Adds the record to the table
    def commit_form(self):
        db.session.add(self)
        db.session.commit()

    # Removes the record from the table
    def remove_form(self):
        db.session.delete(self)
        db.session.commit()

    # Returns a form instance given its id
    @staticmethod
    def get_by_id(id):
        return Form.query.filter(Form.id == id).first()

# Checks if a booking date falls within or equal to a given date
def within_date(given_start,given_end,book_start,book_end):

    if book_start == given_start and book_end == given_end:
        return True
    elif book_start > given_start and book_end < given_end:
        return True
    elif book_start == given_start and book_end < given_end:
        return True
    elif book_start > given_start and book_end == given_end:
        return True
    else:
        return False
