from wtforms import ValidationError
from flask import flash
import datetime
from bicycle_hire.blueprints.bicycle.models import Booking

#Custom date validator
def end_after_start_date_validator(message = "The end date can't come before the start date!"):
    def _end_after_start_date_validator(form, field):
        if form.start_date.data > form.end_date.data:
            raise ValidationError(message=message)

    return _end_after_start_date_validator


def start_after_current_date_validator(message = "The start date can't be in the past!"):
    def _start_after_current_date_validator(form, field):
        if form.start_date.data < datetime.date.today():
            raise ValidationError(message=message)

    return _start_after_current_date_validator


def same_day_equal_start_time_validator(message = "The start datetime can't be the same as the end datetime!"):
    def _same_day_equal_start_time_validator(form, field):
        if form.start_date.data == form.end_date.data:
            if(form.start_time.data == form.end_time.data):
                raise ValidationError(message=message)

    return _same_day_equal_start_time_validator


def same_day_start_after_end_time_validator(message = "The start time can't come after the end time for same day bookings!"):
    def _same_day_start_end_time_validator(form, field):
        if form.end_date.data == form.start_date.data:
            if int(form.start_time.data) > int(form.end_time.data):
                raise ValidationError(message=message)

    return _same_day_start_end_time_validator


def same_day_present_future_validator(message = "Start time can't be in the past!"):
    def _same_day_present_future_validator(form, field):
        if form.start_date.data == datetime.date.today():
            if int(form.start_time.data) < datetime.datetime.now().hour:
                raise ValidationError(message = message)

    return _same_day_present_future_validator


def number_of_bikes_exceeds_available_validator(message = "The number of bikes entered exceeds the number of available bikes in this location!"):
    def _number_of_bikes_exceeds_available_validator(form, field):
        # Get the necessary form data for bicycle availability check
        start_time = form.start_time.data
        end_time = form.end_time.data
        start_date = form.start_date.data
        end_date = form.end_date.data
        location = form.location.data

        # Converts both times and dates into the dateTime format
        start = datetime.datetime.combine(start_date, datetime.time(int(start_time)))
        end = datetime.datetime.combine(end_date, datetime.time(int(end_time)))

        # Gets the number of availalbe bikes in the chosen timeframe and location
        bicycles_available = len(Booking.available_bikes(location, start, end))

        if form.number_of_bikes.data > bicycles_available:
            raise ValidationError(message=message)

    return _number_of_bikes_exceeds_available_validator
