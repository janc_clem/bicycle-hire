from wtforms import SelectField, TimeField, BooleanField, StringField, IntegerField
from wtforms.fields.html5 import DateField
from wtforms.fields import SubmitField
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired, Email
import datetime
from bicycle_hire.blueprints.bicycle.helpers.filter_form_validation import (end_after_start_date_validator,
                                                                            start_after_current_date_validator,
                                                                            same_day_equal_start_time_validator,
                                                                            same_day_start_after_end_time_validator,
                                                                            same_day_present_future_validator,
                                                                            number_of_bikes_exceeds_available_validator)

class FilterForm(FlaskForm):
    location = SelectField('Location', choices=[('London', 'London'), ('Manchester', 'Manchester'),
                                                ('Leeds', 'Leeds'), ('Lancaster', 'Lancaster'),
                                                ('Birmingham', 'Birmingham')])

    start_date = DateField('Start date', default=datetime.date.today(),
                           validators=[DataRequired(message="A starting date selection is needed"),
                                       end_after_start_date_validator()])
    
    end_date = DateField('End date', default=datetime.date.today(),
                         validators=[DataRequired(message="A starting date selection is needed"),
                                     start_after_current_date_validator()])

    start_time = SelectField('Start time',  choices=[('9', '09:00'), ('10', '10:00'),
                                                ('11', '11:00'), ('12', '12:00'),
                                                ('13', '13:00'), ('14', '14:00'),
                                                ('15', '15:00'), ('16', '16:00'),
                                                ('17', '17:00')],
                                            validators=[DataRequired(message="Please select a start time"),
                                                        same_day_equal_start_time_validator(),
                                                        same_day_start_after_end_time_validator(),
                                                        same_day_present_future_validator()])
    
    end_time = SelectField('End time', choices=[('9', '09:00'), ('10', '10:00'),
                                                ('11', '11:00'), ('12', '12:00'),
                                                ('13', '13:00'), ('14', '14:00'),
                                                ('15', '15:00'), ('16', '16:00'),
                                                ('17', '17:00')],
                                                validators=[DataRequired(message="Please select an end time")])
    
    search = SubmitField('Search bikes')
    submit = SubmitField('Book Now')

class BookingFilterForm(FilterForm):

    number_of_bikes = IntegerField('Number of bikes',
     validators=[DataRequired(message="Please select a number of bikes"),
                 number_of_bikes_exceeds_available_validator()])
    submit = SubmitField('Submit')

class AuthForm(FlaskForm):

    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Submit')
