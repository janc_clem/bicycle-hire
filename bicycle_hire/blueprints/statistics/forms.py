from wtforms import SelectField
from wtforms.fields.html5 import DateField
from wtforms.fields import SubmitField
from wtforms.validators import DataRequired
from flask_wtf import FlaskForm
from bicycle_hire.blueprints.bicycle.helpers.filter_form_validation import end_after_start_date_validator
import datetime

def list_weeks():
    weeks = []
    for i in range(1,53):
        weeks.append(('{0}'.format(i),'Week {0}'.format(i)))
    return weeks

class WeekForm(FlaskForm):
    week = SelectField('Week', choices=list_weeks())
    submit = SubmitField('Submit')

class BookedBikesForm(FlaskForm):
    start_date = DateField('Start date', default=datetime.date.today(),
                          validators=[DataRequired(),
                          end_after_start_date_validator()])

    end_date = DateField('End Date', default=datetime.date.today(),
                         validators=[DataRequired()])
    submit = SubmitField('Submit')
