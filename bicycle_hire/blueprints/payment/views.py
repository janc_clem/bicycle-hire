from flask import (Blueprint,
                   render_template,
                   request,
                   flash,
                   redirect,
                   url_for,
                   session
                   )
import stripe
from bicycle_hire.blueprints.payment.models import Payment, Receipt
from bicycle_hire.blueprints.bicycle.models import (Booking,
                                                    Bicycle,
                                                    Location,
                                                    Form)
from flask_weasyprint import HTML, render_pdf
from flask_qrcode import QRcode
from bicycle_hire.extensions import mail
from flask_mail import Message
import random

payments = Blueprint('payments', __name__, template_folder='templates')

stripe_keys = {
    'secret_key': 'sk_test_Fd1sH6mHVc48TCaRvs9Cv1D2',
    'publishable_key': 'pk_test_FTdCPE7dkLllOJMbrKPyuzlW'
}

stripe.api_key = stripe_keys['secret_key']

# "Index" page viewed initially to accept payment info
@payments.route('/payindex')
def payIndex():

    # Payment Information
    payment_id = request.args.get('payment_id')
    form_id = request.args.get('form_id')
    payment_obj = Payment.retrieve_payment_record(payment_id)
    amount = payment_obj.get_payment_amount()

    # Booking Information
    booking = Booking.get_booking_record(payment_obj.get_booking_id())
    email = booking.get_email()
    start = booking.get_start()
    end = booking.get_end()
    bikes = len(booking.get_bikes())
    length = booking.time_in_seconds()
    booking_hours = round(length / 3600)
    initial_cost = booking_hours*bikes*5

    if(booking.is_eligible() == True):
        discount = "-20%"
    else:
        discount = "0%"

    return render_template('payment/payment.html',
                            key = stripe_keys['publishable_key'],
                            amount = amount,
                            stripe_amount = amount*100,
                            payment_id = payment_id,
                            email = email,
                            start = start,
                            end = end,
                            bikes = bikes,
                            form_id = form_id,
                            initial_cost = initial_cost,
                            discount = discount)


# Page that you see upon a successful payment
@payments.route('/thanks')
def thanks():
    # Form removal
    form_id = request.args.get('form_id')
    form_object = Form.get_by_id(form_id)
    if form_object:
        form_object.remove_form()

    # Payment Information
    payment_id = request.args.get('payment_id')
    payment_obj = Payment.retrieve_payment_record(payment_id)
    total = payment_obj.get_payment_amount()

    #Booking Information
    booking = Booking.get_booking_record(payment_obj.get_booking_id())
    start = booking.get_start()
    end = booking.get_end()
    email = booking.get_email()
    bikes = len(booking.get_bikes())
    booking_length = booking.time_in_seconds()
    booking_hours = round(booking_length / 3600)

    bicycle_list = booking.get_bikes()
    location_obj = Location.get_location_record(bicycle_list[0].get_loc_id())
    location = location_obj.get_loc_name()
    initial_cost = booking_hours*bikes*5

    # Check if the bokoing is eligible for a discount and if so apply to receipt
    eligible = booking.is_eligible()

    if(eligible == True):
        discount = "-20%"
    else:
        discount = "0%"

    check = False
    #Creates a random ID number for receipt
    #converted the int to string so that it can be hashed
    id_number = str(random.randint(1, 5000))

    #If the check is false keep assigning random numbers until one is found
    while(check == False):
        #Check if receipt exists
        check = Receipt.check_exist(id_number)
        if(check == True):
            #If it doesn't exist add receipt to table
            receipt = Receipt(id_number, True)
            receipt.add_receipt()
        else:
            id_number = random.randint(1, 5000)

    #assigns the hashed id_number to an object to be used in the website
    hashed = hash(id_number)
    qrcode = QRcode()


    #Stores html template as a PDF
    html = render_template('payment/receipt.html', location = location, start = start,
                           end = end, no_of_bikes = bikes, total = total,
                           discount = discount, initial_cost = initial_cost,
                           hashed=hashed, qrcode = qrcode
                           )
    pdf = HTML(string=html).write_pdf()

    msg_body = "Hi,\nThank you for choosing Bicycle Hire!\n"\
               "You can find the receipt attached to this e-mail.\n"\
               "If you have any enquiries, do not hesitate to contact us.\n\n"\
               "Please do not reply to this automated email."

    msg = Message(subject="Booking Confirmation",
                  recipients=[email],
                  body=msg_body)
    msg.attach(filename="Booking.pdf", content_type = "application/pdf", data=pdf)

    mail.send(msg)

    return render_template("payment/thanks.html")


@payments.route('/pay', methods=['GET','POST'])
def pay():

    # Retrieve form id
    form_id = request.args.get('form_id')

    # Retrieves payment object
    payment_id = request.args.get('payment_id')
    payment_obj = Payment.retrieve_payment_record(payment_id)

    #Updates the 'paid' atribute to True
    payment_obj.update_paid_bool()

    # For charge object amount value
    amount = payment_obj.get_payment_amount()
    amount = int(amount)*100

    # Customer Info
    customer = stripe.Customer.create(email = request.form['stripeEmail'],
                                      source = request.form['stripeToken'])

    # Payment Info
    charge = stripe.Charge.create(
       customer = customer.id,
       amount = amount,
       currency='gbp',
       description ='Bike Hire'
    )

    return redirect(url_for('payments.thanks',
                             payment_id=payment_id,
                             form_id = form_id))
